package CLASES;

import java.sql.Date;

/**
 *
 * @author Febre
 */
public class Detalle {

    private int id_detalle;
    private String nombres;
    private String cedula;
    private String correo;
    private String proyecto;
    private String fechaEntrega;
    private String fechaDevolucion;
    private String usuarios;
    private int id_equipos;

    //Constructor

    public Detalle(int id_detalle, String nombres, String cedula, String correo, String proyecto, String fechaEntrega, String fechaDevolucion, String usuarios, int id_equipos) {
        this.id_detalle = id_detalle;
        this.nombres = nombres;
        this.cedula = cedula;
        this.correo = correo;
        this.proyecto = proyecto;
        this.fechaEntrega = fechaEntrega;
        this.fechaDevolucion = fechaDevolucion;
        this.usuarios = usuarios;
        this.id_equipos = id_equipos;
    }
  

    public Detalle() {
    }

    //Metodos
    public int getId_detalle() {
        return id_detalle;
    }

    public void setId_detalle(int id_detalle) {
        this.id_detalle = id_detalle;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getProyecto() {
        return proyecto;
    }

    public void setProyecto(String proyecto) {
        this.proyecto = proyecto;
    }

    public String getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getFechaDevolucion() {
        return fechaDevolucion;
    }

    public void setFechaDevolucion(String fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    public int getId_equipos() {
        return id_equipos;
    }

    public void setId_equipos(int id_equipos) {
        this.id_equipos = id_equipos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(String usuarios) {
        this.usuarios = usuarios;
    }
    
    
    
    

}
