package CLASES;

/**
 *
 * @author Febre
 */
public class Equipos {

    private int id_equipos;
    private String detalle;
    private String serie;
    private String modelo;
    private String marca;
    private String estado;
    private String pedido;

    //Costrcutor
    public Equipos(int id_equipos, String detalle, String serie, String modelo, String marca, String estado, String pedido) {
        this.id_equipos = id_equipos;
        this.detalle = detalle;
        this.serie = serie;
        this.modelo = modelo;
        this.marca = marca;
        this.estado = estado;
        this.pedido = pedido;
    }

    //Metodos
    public int getId_equipos() {
        return id_equipos;
    }

    public void setId_equipos(int id_equipos) {
        this.id_equipos = id_equipos;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPedido() {
        return pedido;
    }

    public void setPedido(String pedido) {
        this.pedido = pedido;
    }

}
