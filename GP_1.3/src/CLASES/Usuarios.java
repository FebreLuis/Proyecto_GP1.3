package CLASES;

/**
 *
 * @author Febre
 */
public class Usuarios {

    private int id_usuario;
    private String cedula;
    private String nombres;
    private String usuario;
    private String contrasena;
    private String permiso;

    //Constructor
    public Usuarios() {
    }

    public Usuarios(int id_usuario, String cedula, String nombres, String usuario, String contrasena, String permiso) {
        this.id_usuario = id_usuario;
        this.cedula = cedula;
        this.nombres = nombres;
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.permiso = permiso;
    }

    //Metodos
    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getPermiso() {
        return permiso;
    }

    public void setPermiso(String permiso) {
        this.permiso = permiso;
    }

}
