package BL;

import CLASES.Detalle;
import CLASES.Equipos;
import CLASES.Usuarios;
import DAT.DATUtpl;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Febre
 */
public class BLUtpl {

    DATUtpl data = new DATUtpl();

    //INGRESAR
    public int ingresarEquipos(Equipos objEqui) {

        return data.ingresarEquipos(objEqui.getDetalle(), objEqui.getSerie(), objEqui.getModelo(), objEqui.getMarca(), objEqui.getEstado(), objEqui.getPedido());

    }

    public int ingresarDetalle(Detalle objDeta) {

        return data.ingresarDetalle(objDeta.getNombres(), objDeta.getCedula(), objDeta.getCorreo(), objDeta.getProyecto(), objDeta.getFechaEntrega(), objDeta.getFechaDevolucion(), objDeta.getUsuarios(), objDeta.getId_equipos());

    }
    
      public int ingresarUsuarios(Usuarios objUsu) {

        return data.ingresarUsuario(objUsu.getCedula(),objUsu.getNombres(),objUsu.getUsuario(),objUsu.getContrasena(),objUsu.getPermiso());

    }

    //CONSULTAR
    public ArrayList<Usuarios> ConsultarUsuario() throws ClassNotFoundException, SQLException {
        ArrayList<Usuarios> lstUsuario = new ArrayList<>();
        ResultSet rs = data.ConsultarPersona();
        while (rs.next()) {
            int id = rs.getInt("id_usuario");
            String cedula = rs.getString("cedula");
            String nombre = rs.getString("nombres");
            String usuario = rs.getString("usuario");
            String password = rs.getString("contrasena");
            String permiso = rs.getString("permiso");

            Usuarios objPe = new Usuarios(id, cedula, nombre, usuario, password, permiso);

            lstUsuario.add(objPe);

        }
        return lstUsuario;
    }
     public ResultSet consultarUsuario2() throws ClassNotFoundException, SQLException {
        ResultSet rs = data.ConsultarPersona();
        return rs;
    }

    public ResultSet consultarEquipos() {
        ResultSet rs = data.consultarEquipos();
        return rs;
    }

    public ResultSet consultarDetalle2() throws ClassNotFoundException, SQLException {
        ResultSet rs = data.ConsultarDetalle2();
        return rs;
    }

    public ArrayList<Detalle> ConsultarDetalle() throws ClassNotFoundException, SQLException {
        ArrayList<Detalle> lstDetalle = new ArrayList<>();
        ResultSet rs = data.consultarDetalle();
        while (rs.next()) {
            int id = rs.getInt("id_detalle");
            String nombres = rs.getString("nombres");
            String cedula = rs.getString("cedula");
            String correo = rs.getString("correo");
            String tipoPro = rs.getString("tipo_proyecto");
            String fechaEntrega = rs.getString("fechaEntrega");
            String fechaDevolucion = rs.getString("fechaDevolucion");
            String usuario = rs.getString("usuario");
            int id_Equipo = rs.getInt("id_equipos");

            Detalle objDe = new Detalle(id_Equipo, nombres, cedula, correo, tipoPro, fechaEntrega, fechaDevolucion, usuario, id_Equipo);

            lstDetalle.add(objDe);

        }
        return lstDetalle;
    }

    public ArrayList<Equipos> ConsultarEquipos2() throws ClassNotFoundException, SQLException {
        ArrayList<Equipos> lstEquipos = new ArrayList<>();
        ResultSet rs = data.consultarEquipos();
        while (rs.next()) {
            int id = rs.getInt("id_equipos");
            String detalle = rs.getString("detalle");
            String serie = rs.getString("serie");
            String modelo = rs.getString("modelo");
            String marca = rs.getString("marca");
            String estado = rs.getString("estado");
            String pedido = rs.getString("pedido");

            Equipos objDe = new Equipos(id, detalle, serie, modelo, marca, estado, pedido);

            lstEquipos.add(objDe);

        }
        return lstEquipos;
    }

    //MODIFICAR
    public int modificarEquipos(String detalle, String serie, String modelo, String marca, String estado, int id) {

        int retorno = data.modificarEquipos(detalle, serie, modelo, marca, estado, id);
        return retorno;
    }
    
     public int modificarUsuario(String permiso, int id) {

        int retorno = data.modificarUsuario(permiso, id);
        return retorno;
    }

    public int modificarPedido(String pedido, int id) {
        int retorno = data.modificarPedido(pedido, id);
        return retorno;
    }

}
