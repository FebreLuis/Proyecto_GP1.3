package DAT;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Febre
 */
public class DATUtpl {

    DATConexion data = new DATConexion();

    //INGRESOS
    public int ingresarEquipos(String detalle, String serie, String modelo, String marca, String estado, String pedido) {
        int retorno = 0;
        String sentencia = "INSERT INTO equipos (detalle, serie, modelo, marca,estado,pedido) VALUES (?,?,?,?,?,?)";
        try {
            PreparedStatement pst = data.AbrirConexion().prepareStatement(sentencia);
            pst.setString(1, detalle);
            pst.setString(2, serie);
            pst.setString(3, modelo);
            pst.setString(4, marca);
            pst.setString(5, estado);
            pst.setString(6, pedido);
            retorno = pst.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        }
        return retorno;
    }

    public int ingresarDetalle(String nombres, String cedula, String correo, String proyecto, String fechaEntre, String fechaDevo, String usuario, int id_Equipos) {
        int retorno = 0;
        String sentencia = "INSERT INTO detalle (nombres, cedula,correo,tipo_proyecto,fechaEntrega,fechaDevolucion,usuario,id_equipos) VALUES (?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = data.AbrirConexion().prepareStatement(sentencia);
            pst.setString(1, nombres);
            pst.setString(2, cedula);
            pst.setString(3, correo);
            pst.setString(4, proyecto);
            pst.setString(5, fechaEntre);
            pst.setString(6, fechaDevo);
            pst.setString(7, usuario);
            pst.setInt(8, id_Equipos);
            retorno = pst.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        }
        return retorno;
    }

    public int ingresarUsuario(String cedula, String nombres, String usuario, String contrasena, String permiso) {
        int retorno = 0;
        String sentencia = "INSERT INTO usuario (cedula,nombres,usuario,contrasena,permiso) VALUES (?,?,?,?,?)";
        try {
            PreparedStatement pst = data.AbrirConexion().prepareStatement(sentencia);
            pst.setString(1, cedula);
            pst.setString(2, nombres);
            pst.setString(3, usuario);
            pst.setString(4, contrasena);
            pst.setString(5, permiso);

            retorno = pst.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        }
        return retorno;
    }

    //CONCULTAS
    public ResultSet ConsultarPersona() throws ClassNotFoundException, SQLException {
        PreparedStatement pst = data.AbrirConexion().prepareStatement("SELECT * FROM usuario");//obj para poder manipular abre coneccion y crea unStatement 
        //crea un a varible y ponermos la cadena para recuperar todo desde la BDD
        ResultSet rs = pst.executeQuery();//recuper un un ResultSet y envio la varible a executeQuery
        return rs;//retornma en un obj ResultSet rs
    }

    public ResultSet ConsultarDetalle2() throws ClassNotFoundException, SQLException {
        PreparedStatement pst = data.AbrirConexion().prepareStatement("SELECT * FROM detalle");//obj para poder manipular abre coneccion y crea unStatement 
        //crea un a varible y ponermos la cadena para recuperar todo desde la BDD
        ResultSet rs = pst.executeQuery();//recuper un un ResultSet y envio la varible a executeQuery
        return rs;//retornma en un obj ResultSet rs
    }

    public ResultSet consultarEquipos() {
        ResultSet rs = null;
        try {
            PreparedStatement pre = data.AbrirConexion().prepareStatement("SELECT * FROM equipos; ");
            rs = pre.executeQuery();//recupera un un ResultSet y envio la varible a executeQuery

        } catch (Exception e) {
            System.out.println(e);
        }
        return rs;//retornma en un obj ResultSet rs
    }

    public ResultSet consultarDetalle() {
        ResultSet rs = null;
        try {
            PreparedStatement pre = data.AbrirConexion().prepareStatement("SELECT * FROM detalle; ");
            rs = pre.executeQuery();//recupera un un ResultSet y envio la varible a executeQuery

        } catch (Exception e) {
            System.out.println(e);
        }
        return rs;//retornma en un obj ResultSet rs
    }

    //MODIFICAR
    public int modificarEquipos(String detalle, String serie, String modelo, String marca, String estado, int id) {
        int retorno = 0;

        String sentencia = ("update equipos set detalle=?, serie=?, modelo=?, marca=?, estado=? where id_equipos =?");
        try {

            PreparedStatement pst = data.AbrirConexion().prepareStatement(sentencia);
            pst.setString(1, detalle);
            pst.setString(2, serie);
            pst.setString(3, modelo);
            pst.setString(4, marca);
            pst.setString(5, estado);
            pst.setInt(6, id);
            retorno = pst.executeUpdate();
            pst.close();

        } catch (Exception e) {
        }
        return retorno;
    }

    public int modificarPedido(String pedido, int id) {
        int retorno = 0;

        String sentencia = ("update equipos set pedido=? where id_equipos =?");
        try {

            PreparedStatement pst = data.AbrirConexion().prepareStatement(sentencia);
            pst.setString(1, pedido);
            pst.setInt(2, id);
            retorno = pst.executeUpdate();
            pst.close();

        } catch (Exception e) {
        }
        return retorno;
    }
    
      public int modificarUsuario(String permiso, int id) {
        int retorno = 0;

        String sentencia = ("update usuario set permiso=? where id_usuario=?");
        try {

            PreparedStatement pst = data.AbrirConexion().prepareStatement(sentencia);
            pst.setString(1, permiso);
            pst.setInt(2, id);
            retorno = pst.executeUpdate();
            pst.close();

        } catch (Exception e) {
        }
        return retorno;
    }

}
